package com.college.view;


import java.util.List;
import java.util.Scanner;

import com.college.contoller.AdminController;
import com.college.model.Detail;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter user id");
		String id = sc.nextLine();
		System.out.println("Enter password");
		String pass = sc.nextLine();
		AdminController adc = new AdminController();
		int result=0;
		int s=0;
		result = adc.adminAuthentication(id,pass);
		if(result>0) {
			System.out.println("Hi "+id+" Welcome to Home page");
			do {
			System.out.println("1. Add StudentData , 2.Remove StudentData , 3.Edit StudentData , 4.View All StudentData");
			System.out.println("Enter the option");
			int opt = sc.nextInt();
			sc.nextLine();
			String rollno =" ";
			String name =" ";
			String deptartment =" ";
			Long contact=0l;
			
			switch(opt) {
			case 1:
				System.out.println("Enter student Rollno,name,dob,department,yearofjoining,yearodpassedout,bloodgoup,address,contact");
				
				String[] data = sc.nextLine().split(",");
				result = adc.addData(data[0],data[1], data[2], data[3], data[4], data[5], data[6],data[7], Long.parseLong(data[8]));
				if(result>0) {
					System.out.println(data[0]+" Added Sucessfully");
				}
				break;
			case 2:
				System.out.println("Enter the rollno");
				rollno = sc.nextLine();
				adc.removeData(rollno);
				System.out.println((result>0)?rollno+" Deleted sucessfully ": rollno+" No Deletion");
				break;
			case 3:
				System.out.println("1.Edit Name, 2.Edit Dept, 3.Edit Contact,4.Edit Name & Dept & Contact");
				System.out.println("Enter the option");
				int opt1 =sc.nextInt();
				sc.nextLine();
				if(opt1 == 1) {
					System.out.println("Enter the rollno");
					rollno = sc.nextLine();
					System.out.println("Enter the name");
					name = sc.nextLine();
					adc.updateName(rollno, name);
					System.out.println((result>0)?name+" update sucessfully ": name+" Not Update Sucessfully");
				}
				else if (opt1 ==2) {
					System.out.println("Enter the rollno");
					rollno = sc.nextLine();
					System.out.println("Enter the Dept");
					deptartment = sc.nextLine();
					adc.updateDept(rollno, deptartment);
					System.out.println((result>0)?deptartment+" update sucessfully ": deptartment+" Not Update Sucessfully");
				}
				else if (opt1 == 3) {
					System.out.println("Enter the rollno");
					rollno = sc.nextLine();
					System.out.println("Enter the contact");
					contact =sc.nextLong();
					adc.updateContact(rollno, contact);
					System.out.println((result>0)?contact+" update sucessfully ": contact+" Not Update Sucessfully");
				}
				else if (opt1 == 4) {
					System.out.println("Enter the rollno");
					rollno = sc.nextLine();
					System.out.println("Enter the name");
					name = sc.nextLine();
					System.out.println("Enter the Dept");
					deptartment = sc.nextLine();
					System.out.println("Enter the contact");
					contact =sc.nextLong();
					adc.updateInfo(rollno, name, deptartment, contact);
					System.out.println((result>0)?"Info update sucessfully ": "Info Not Update Sucessfully");
				}
				else
					System.out.println("invaid option");
				break;
			case 4:
				List<Detail> list= adc.viewAllDetail();
				if(list.size()>0) {
					System.out.println("Rollno,name,dob,department,yearofjoining,yearodpassedout,bloodgoup,address,contact");
					for (Detail detail : list) {
						System.out.println(detail.getRollno()+","+detail.getName()+","+detail.getDob()+","+detail.getDepartment()+","+
								detail.getYearofjoining()+","+detail.getYearofpassedout()+","+detail.getBloodgroup()+","+
								detail.getAddress()+","+detail.getContact());
					}
				}
				else
					System.out.println("Empty student details");
				break;
			default:
				System.out.println("invalid option");
				break;
			}
			System.out.println("would you like to continue press 1");
			 s =sc.nextInt();
			}while(s == 1);
			System.out.println("Process completed!!!!");
		}
		else {
			System.out.println("Incorrect id or password");
		}
			sc.close();
	}
}
