package com.college.dao;

import java.util.List;

import com.college.model.Admin;
import com.college.model.Detail;

public interface Admindao {
	public int adminAuthentication(Admin admin);
	public List<Detail> viewAllDetail();
	public int addData(Detail detail);
	public int updateName(Detail detail);
	public int updateDept(Detail detail);
	public int updateContact(Detail detail);
	public int updateInfo(Detail detail);
	public int removeData(Detail detail);
}
