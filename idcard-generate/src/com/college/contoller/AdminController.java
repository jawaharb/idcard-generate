package com.college.contoller;

import java.util.List;

import com.college.dao.AdminImp;
import com.college.dao.Admindao;
import com.college.model.Admin;
import com.college.model.Detail;

public class AdminController {
	int result;
	Admindao ad = new AdminImp();
	public int adminAuthentication(String uid, String password) {
		Admin admin = new Admin(uid, password);
		return ad.adminAuthentication(admin);
	}
	
	public List<Detail> viewAllDetail(){
		return ad.viewAllDetail();
	}
	
	public int addData(String rollno, String name, String dob, String department, String yearofjoining,
			String yearofpassedout, String bloodgroup, String address, Long contact) {
		Detail detail = new Detail(rollno,name,dob,department,yearofjoining,yearofpassedout,bloodgroup,address,contact);
		return ad.addData(detail);
	}
	
	public int updateName(String rollno,String name) {
		Detail detail = new Detail();
		detail.setRollno(rollno);
		detail.setName(name);
		return ad.updateName(detail);
		
	}
	
	public int updateDept(String rollno, String deptarment) {
		Detail detail = new Detail();
		detail.setRollno(rollno);
		detail.setDepartment(deptarment);
		return ad.updateDept(detail);
		
	}
	
	public int updateContact(String rollno,Long contact) {
		Detail detail = new Detail();
		detail.setRollno(rollno);
		detail.setContact(contact);
		return ad.updateContact(detail);
		
	}
	
	public int updateInfo(String rollno,String name,String deptarment,Long contact) {
		Detail detail = new Detail();
		detail.setRollno(rollno);
		detail.setName(name);
		detail.setDepartment(deptarment);
		detail.setContact(contact);
		return ad.updateInfo(detail);
	}
	public int removeData(String rollno) {
		Detail detail = new Detail();
		detail.setRollno(rollno);
		return ad.removeData(detail);
	}
}
